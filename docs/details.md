# Additional syntax and formatting

Special syntax (through HTML comments) is available for adding attributes to Markdown elements

## New slide
  * Markdown - three empty lines or separate .md file
    ```
    # My first slide title
    Content of my first slide
    <empty line>
    <empty line>
    <empty line>
    # My second slide title
    Content of my seconds slide
    ```
    
  * HTML syntax - \<section\> element
    ```
    <section>
    # My first slide title
    Content of my first slide
    </section>
    <section>
    # My second slide title
    Content of my second slide
    </section>
    ```
    
## Fragmentation
It is possible to adjust behavior of fragments of your slide, e.g. order in which the elements are shown can be defined by class *fragment* with optional attribute data-fragment-index:
  * Markdown
  ```
    * first element  <!-- .element: class="fragment"-->
  <!-- .element: class="fragment" data-fragment-index="3" -->
    * third element
  <!-- .element: class="fragment" data-fragment-index="2" -->
    * second element
  ```
  * HTML syntax
  ```
  ...
  <div class="fragment" data-fragment-index="3">
    third element
  </div>
  ...
  ```
  
Additional classes extending class fragment:  

  * special fragment entry: fade-in-then-semi-out, fade-in-then-out, semi-fade-out, zoom-in, fade-up, fade-left, ...
  
  * highlighting:  
    * highlight-\<color\>
    
      ```
      My text which will get red <!-- .element: class="fragment highlight-red" -->
      ```
      
    * highlight-current-\<color\>  
    ```
    My text which will get red and black again<!-- .element: class="fragment highlight-current-red" -->
    ```
    
  * hiding fragments - class current-visible  
  
  ```
  My text which appears and then disappears<!-- .element: class="fragment current-visible" -->
  ```
    
  * simple animation features: grow, shrink, strike  
    
  ```
  My text which will be striked out<!-- .element: class="fragment strike" -->
  ```

# External resources

For more tips on supported functionality visit official [reveal.js GitHub repository](https://github.com/hakimel/reveal.js/).